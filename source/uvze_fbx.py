'''
This file is part of UVZE add-on.
    UVZE is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, see <https://www.gnu.org/licenses>.

    # Script copyright (C) José Esteban
    # It uses code from the add-on 'io_scene_fbx', which is the property of its respective authors. 
''' 

from io_scene_fbx import ExportFBX
from bpy.props import BoolProperty
import bpy
from . uvze import (UVZE)

class UVZE_ExportFBX(ExportFBX):
    """Write a FBX file (overrided by uvze)"""
    
    enable_uvze: BoolProperty(
        name="Enable",
        description="Expand uvs with 0 scale",
        default=False
    )

    def execute(self, context):
        uvze = UVZE(save_for_restore=True)

        if self.enable_uvze and bpy.context.object.mode != "OBJECT":
            self.report({'INFO'}, 'Change mode to OBJECT and export again')
            return {'CANCELLED'}

        if self.enable_uvze:
            uvze.expand_uvs(context, self.use_selection)
        
        result = super().execute(context)

        if self.enable_uvze:
            uvze.restore_uvs()

        return result

class UVZE_PT_FBX_export_panel(bpy.types.Panel):
    bl_space_type = 'FILE_BROWSER'
    bl_region_type = 'TOOL_PROPS'
    bl_label = "UVZE_p"
    bl_parent_id = "FILE_PT_operator"
    bl_options = {'HIDE_HEADER'}

    def draw(self, context):       
        layout = self.layout
        sfile = context.space_data
        operator = sfile.active_operator

        col = layout.column(heading="UV zero expander")
        col.prop(operator, "enable_uvze")
  