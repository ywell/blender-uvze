<div align="center">
  <br>
  <h1>UVZE</h1>
  <strong>A blender add-on</strong>
</div>
<br>

## What is UVZE?
UVZE is an add-on for blender that exapands the uvs with zero scale. Useful when you have a texture with a colormap and you set the uvs of a polygon in a particular region and it scales with a value of zero.

<img src="doc/8x8-map-example-blender.png" height="200">

Some game engines may display warnings if uvs are scaled to zero, like Unreal Engine, which displays
``there are some nearly zero bi-normals which can create some issues. (Tolerance of 1E-4) ``

## Getting Started
Download <a href="https://gitlab.com/ywell/blender-uvze/uploads/ac3bad0075cb3643a67dd4a442aaaee7/uvze_1.0.0.zip" target="_blank" rel="noreferrer">uvze_1.0.0.zip</a>, open blender, install and activate it.

## Usage
A panel is added in the 3d view in object mode, it contains a checkbox and a button to execute the operator that will perform the operation of expanding the uvs with zero scale.

<img src="doc/uvze-panel.png" width="200">

Also, if you have the Export-Inport FBX add-on enabled, it adds an option at the end in the export to expand the scaled uvs to zero and once the export is finished it returns the uvs positions to their original place.

<img src="doc/uvze-fbx-export.png" width="100">

## Considerations
- Tested only with blender 3.4 under Windows 10.
- At the moment it only allows the operation on the first uv channel.
- The add-on is intended and used on "low poly style" models, so it has not been tested with large scenes or with a high number of polygons.
- If you have the Export-Import FBX add-on enabled and you disable or delete UVZE, the FBX export will stop showing in both the export menu and in the search until blender is restarted.