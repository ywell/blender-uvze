'''
This file is part of UVZE add-on.
    UVZE is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, see <https://www.gnu.org/licenses>.

    # Script copyright (C) José Esteban
''' 

bl_info = {
    "name": "UV Zero Expander",
    "author": "jesteban",
    "version": (1, 0, 0),
    "blender": (3, 4, 0),
    "location": "",
    "description": "Expand UV with zero scale.",
    "warning": "",
    "doc_url": "https://gitlab.com/ywell/blender-uvze",
    "category": "",
}

# if FBX addon found, import functionality
import importlib
fbx_loader = importlib.find_loader('io_scene_fbx')
use_in_fbx_export = fbx_loader is not None
if use_in_fbx_export is not None:
    from . uvze_fbx import (UVZE_ExportFBX, UVZE_PT_FBX_export_panel)

import bpy
from . uvze_preferences import UVZEPreferences 
from . uvze_op_expand import UVZE_OT_expand
from . uvze_panel import UVZE_PT_Panel

classes = (
    UVZEPreferences,
    UVZE_OT_expand,
    UVZE_PT_Panel
)

# if FBX addon found, register classes
if use_in_fbx_export:
    classes = (UVZE_ExportFBX, UVZE_PT_FBX_export_panel) + classes

# register for search
def register_search(self, context):
    self.layout.operator(UVZE_OT_expand.bl_idname)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.VIEW3D_MT_object.append(register_search)

def unregister():
    bpy.types.VIEW3D_MT_object.remove(register_search)

    for cls in classes:
        bpy.utils.unregister_class(cls)

if __name__ == "__main__":
    register()