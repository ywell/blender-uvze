'''
This file is part of UVZE add-on.
    UVZE is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, see <https://www.gnu.org/licenses>.

    # Script copyright (C) José Esteban
''' 

from typing import NamedTuple
from itertools import groupby
from mathutils import Vector
import math
import bpy

class ObjectPolygonData(NamedTuple):
    poly_index: int
    location: Vector

class ObjectData(NamedTuple): 
    obj: bpy.types.Object
    polygons: list[ObjectPolygonData]

class UVZE: 
    def __init__(self, index: int = 0, save_for_restore: bool = False):
        self.index = index
        self.save_for_restore = save_for_restore

    radius:float = .003
    objects_data: list[ObjectData] = []

    def all_equal(self, iterable):
        """Check if all values are equal"""

        grouped = groupby(iterable)
        return next(grouped, True) and not next(grouped, False)            
    
    def expand_uvs(self, context, use_selection):
        """Expand uvs with zero scale arround a circle center"""

        if use_selection:
            objects = context.selected_objects
        else:
            objects = context.view_layer.objects  

        # gather objects
        objects = tuple(obj for obj in objects if obj.visible_get() and obj.type == "MESH")
        for obj in objects:

            # store object if needed
            if self.save_for_restore:
                object_data = ObjectData(obj, [])

            data = obj.data
            uv_layer = data.uv_layers[self.index].data
            for poly in data.polygons: 

                # get uv vectors
                uvs = [uv_layer[i].uv for i in range(poly.loop_start, poly.loop_start + poly.loop_total)]

                # if all uv locations are equal, then proceed
                if self.all_equal(uvs):
                    poly_data = ObjectPolygonData(poly.index, uvs[0].copy())

                    # save original locations if needed
                    if self.save_for_restore:
                        object_data.polygons.append(poly_data)
                        self.objects_data.append(object_data)

                    degrees_step = 360 / poly.loop_total
                    i = 0
                    for loop_index in range(poly.loop_start, poly.loop_start + poly.loop_total): 
                        angle = math.radians(i * degrees_step)
                        uv_layer[loop_index].uv.x = poly_data.location.x + (self.radius * math.cos(angle))
                        uv_layer[loop_index].uv.y = poly_data.location.y + (self.radius * math.sin(angle))
                        i += 1               
    
    def restore_uvs(self):
        """Restore original uvs positions"""
        
        if self.save_for_restore == False:
            return

        for obj_data in self.objects_data:
            uv_layer = obj_data.obj.data.uv_layers[self.index].data
            for poly_data in obj_data.polygons:
                poly = obj_data.obj.data.polygons[poly_data.poly_index]
                for loop_index in range(poly.loop_start, poly.loop_start + poly.loop_total): 
                    uv_layer[loop_index].uv.x = poly_data.location.x
                    uv_layer[loop_index].uv.y = poly_data.location.y
                    
        del self.objects_data[:]
 