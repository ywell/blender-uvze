'''
This file is part of UVZE add-on.
    UVZE is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, see <https://www.gnu.org/licenses>.

    # Script copyright (C) José Esteban
''' 

from . uvze_preferences import UVZEPreferences
from . uvze_op_expand import UVZE_OT_expand 
import bpy

class UVZE_PT_Panel(bpy.types.Panel):
    bl_label = "UVZE"
    bl_idname = "UVZE_PT_Panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'UVZE'
    bl_context = "objectmode"
    
    @classmethod
    def poll(self,context):
        return context.object is not None
    
    def draw(self, context):
        layout = self.layout
        prefs:UVZEPreferences = context.preferences.addons[__package__].preferences

        col = layout.column(align=True)
        col.prop(prefs, "panel_use_selection")
        col = layout.column(align=True)
        props = col.operator(UVZE_OT_expand.bl_idname, text="Expand UV", icon="UV")
        props.use_selection = prefs.panel_use_selection