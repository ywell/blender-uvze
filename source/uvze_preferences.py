'''
This file is part of UVZE add-on.
    UVZE is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 3
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, see <https://www.gnu.org/licenses>.

    # Script copyright (C) José Esteban
''' 

from bpy.props import BoolProperty
import bpy

class UVZEPreferences(bpy.types.AddonPreferences):
    bl_idname = __package__

    panel_use_selection: BoolProperty(
        name="Only selected objects",
        description="Only selected objects",
        default=True
    )